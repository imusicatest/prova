import { Component , OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  textoRodape: string = '';

  ngOnInit() {
    this.textoRodape = 'Lorem ipsum dolor sit amet, consectetur adipiscing '
                     + 'elit, sed do eiusmod tempor incididunt ut labore et '
                     + 'dolore magna aliqua. Ut enim ad minim veniam, quis '
                     + 'nostrud exercitation ullamco laboris nisi ut aliquip '
                     + 'ex ea commodo consequat.';

  }
}
