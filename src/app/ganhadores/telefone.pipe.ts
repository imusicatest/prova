import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'telefone'
})
export class TelefonePipe implements PipeTransform {

  transform(value: string): string {
    let tl: string[] = [];
    let tel: string = '';
    let telefone: string = '';
    tl = value.split('-');
    telefone = tl[0];
    for ( let t = 0; t < tl[1].length ; t++ ) {
      tel += 'x';
    }
    telefone += '-' + tel;
    return telefone;
  }

}
