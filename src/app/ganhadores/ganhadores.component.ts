import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

interface Ganhadores {
  winners: any;
}

@Component({
  selector: 'app-ganhadores',
  templateUrl: './ganhadores.component.html',
  styleUrls: ['./ganhadores.component.scss']
})
export class GanhadoresComponent implements OnInit {
  premioS: string = 'Prêmio Semanal';
  premioT: string = 'Prêmio Trimestral';
  cabecalho: string[] = ['Data', 'Participante', 'Celular', 'Prêmio'];
  url: string = '../../assets/ganhadores.json';
  semanal: any;
  trimestral: any;
  premioSemanal: string;
  premioTrimestral: string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.loadGanhadores();
  }

  loadGanhadores() {
   return  this.http.get<Ganhadores>(this.url).subscribe(
      res => {
        console.log(res);
        this.semanal = res.winners.weekly.users;
        this.trimestral = res.winners.quarterly.users;
        this.premioSemanal = res.winners.weekly.award;
        this.premioTrimestral = res.winners.quarterly.award;
    },
    (err: HttpErrorResponse) => {
      console.log(err.error);
      console.log(err.name);
      console.log(err.message);
      console.log(err.status);
    }
  );
  }


}
