import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { routing } from './app.routing';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GanhadoresComponent } from './ganhadores/ganhadores.component';
import { TelefonePipe } from './ganhadores/telefone.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GanhadoresComponent,
    TelefonePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
