import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { GanhadoresComponent } from './ganhadores/ganhadores.component';


const APP_ROUTES: Routes = [
    { path: '', component: HomeComponent},
    { path: 'home', component: HomeComponent},
    { path: 'ganhadores', component: GanhadoresComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
