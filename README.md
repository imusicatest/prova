# imusica

O projeto foi feito com Angular 5 [Angular CLI](https://github.com/angular/angular-cli) version 1.6.8.

Portanto, você precisa tê-lo instalado:

> sudo npm install @angular/cli -g

# Para configurar e rodar em localhost:4200  =>  siga os passos abaixo
------

## Instale as dependências do projeto

> sudo npm install

## Para rodar no ambiente de desenvolvimento faça:

> sudo ng serve

Isso irá rodar o projeto na url: `http://localhost:4200/`.

A aplicação irá recarregar automaticamente a cada alteração no código.

As alterações nos arquivos Sass(.scss) também são compiladas a cada alteração gerando o css automaticamente.

## Para gerar novos componentes, services, diretivas, interfaces, modulos etc, faça:

Dentro de ./src/app/

> sudo ng generate component nome-do-componente

ou

> sudo ng g c nome-do-componente

Você pode gerar ainda: `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Para fazer o build para produção:

> sudo ng build --prod

Isso cria o diretório ./dist com arquivos minificados

 ou

> sudo ng build --prod --build-optimizer

Isso irá gerar o build de produção do projeto na pasta dist/. com os arquivos otimizados para a produção.
Somente os arquivos gerados em ./dist vão para homologação/produção.

## Para rodar localmente com host configurado, siga estes passos:

Defina um host de sua preferência:

```
/etc/hosts

```
> 127.0.0.1 dev.imusica.com.br

Configure um servidor. Exemplo: Apache2
-------

```
<VirtualHost *:80>
    ServerName dev.imusica.com.br
    ServerAlias www.dev.imusica.com.br

    ### Coloque o caminho do seu web server local
    DocumentRoot "/Library/WebServer/Documents/imusica/dist"

    ErrorLog "/private/var/log/apache2/dev.imusica.com.br-error_log"
    CustomLog "/private/var/log/apache2/dev.imusica.com.br-access_log" common

    RewriteEngine On      
      RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
      RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
      RewriteRule ^ - [L]
      # Para as rotas funcionarem, apontam as requisições para o index
      RewriteRule ^ /index.html

    ### Coloque o caminho do seu web server local
   <Directory "/Library/WebServer/Documents/dev.imusica.com.br/dist">
        Require all granted
        Options Includes FollowSymLinks
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>

</VirtualHost>
```

Para o ambiente final de produção, lembre-se de realizar essas configurações no servidor de produção:
----

## Configurações para o servidor de produção

Seguindo a documentação do Angular, siga essas instruções de acordo com o seu servidor: Apache, Ngnix ou IIS

 => https://angular.io/guide/deployment#fallback
